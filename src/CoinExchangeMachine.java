public class CoinExchangeMachine {

    private int wallet;
    private String name;
    public CoinExchangeMachine(int wallet, String name){
        this.wallet = wallet;
        this.name = name;
    }
    public int wallet(){
        return wallet;
    }
    public String name(){
        return name;
    }
    public static void CoinMenu(){
        System.out.println("|*********************|");
        System.out.println("| 10 Bath = 1 Gcoin   |");
        System.out.println("| 20 Bath = 2 Gcoin   |");
        System.out.println("| 50 Bath = 5 Gcoin   |");
        System.out.println("| 100 Bath = 10 Gcoin |");
        System.out.println("|*********************|");
    }
    public void CoinExchange(){
        System.out.println(name + " have receive " + wallet/10 + " Gcoin");
    }
}

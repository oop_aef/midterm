public class GameMachine {
    public static void main(String[] args) {
        CoinExchangeMachine AEF = new CoinExchangeMachine(30, "AEF");
        AEF.CoinMenu();
        AEF.CoinExchange();
        TimeGame AEF1 = new TimeGame(3, 15);
        AEF1.ShowTime();

        CoinExchangeMachine Shiba = new CoinExchangeMachine(1000, "Shiba");
        Shiba.CoinMenu();
        Shiba.CoinExchange();
        TimeGame Shiba1 = new TimeGame(100, 15);
        Shiba1.ShowTime();

        CoinExchangeMachine Ray = new CoinExchangeMachine(500, "Ray");
        Ray.CoinMenu();
        Ray.CoinExchange();
        TimeGame Ray1 = new TimeGame(50, 15);
        Ray1.ShowTime();
    }
}

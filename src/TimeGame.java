public class TimeGame {
    private int coin;
    private int time;
    public TimeGame(int coin, int time){
        this.coin = coin;
        this.time = time;
    }
    public int getCoin() {
        return coin;
    }
    public int gettime() {
        return time;
    }
    public void ShowTime() {
        System.out.println("Time to play " + coin*time + " Minute");
        System.out.println();
        System.out.println();
    }
}
